
## [v1.2.0](https://bitbucket.org/roitinnovation/$NAMEAPP/compare/v1.2.0..v1.1.0)

2021-05-19

### Chore

* **changelog:** generated CHANGELOG.md file

### Feat

* **service-mesh:** Adding new uniqueIdentifier in logger.

### Pull Requests

* Merged in develop (pull request [#5](https://bitbucket.org/roitinnovation//issues/5/))
* Merged in feature/AIT-2266 (pull request [#4](https://bitbucket.org/roitinnovation//issues/4/))


## [v1.1.0](https://bitbucket.org/roitinnovation/$NAMEAPP/compare/v1.1.0..v1.0.0)

2021-04-22

### Chore

* **changelog:** generated CHANGELOG.md file
* **log-api-route:** update name in setup from Logger to api-ai-logger, adjusted description
* **log-api-route:** removed .egg-info folder with caches

### Feat

* **log-api-route:** added api route request and added the strategy for saving unknown errors

### Pull Requests

* Merged in develop (pull request [#3](https://bitbucket.org/roitinnovation//issues/3/))
* Merged in feature/AIT-1991 (pull request [#2](https://bitbucket.org/roitinnovation//issues/2/))
* Merged in feature/AIT-1991 (pull request [#1](https://bitbucket.org/roitinnovation//issues/1/))


## v1.0.0

2021-04-22

### Chore

* **readme:** Adding info's to readme

### Feat

* **logger:** Adding error to logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger
* **logger:** First commit with pipeline of logger

### Fix

* **environment:** Adding envrionment to logging
* **requirements:** Removing easydict from requirements

