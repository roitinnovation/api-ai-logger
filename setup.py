from setuptools import setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name = "api-ai-logger",
    url='https://bitbucket.org/roitinnovation/api-ai-logger',
    author = "Victor Cavalhere",
    author_email = "victor.cavalhere@roit.com",
    packages=['logger'],
    install_requires = required,
    version = "1.0.0",
    license='MIT',
    description = ("API Logger to Python Projects in ROIT"),
)
