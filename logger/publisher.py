import json
import time

from google.cloud import pubsub_v1


class Publisher:
    def __init__(self, cfg):
        self.publisher = pubsub_v1.PublisherClient()
        self.topic = cfg.logging_topic

    def push(self, message):
        message = json.dumps(message)

        res = self.publisher.publish(self.topic, message.encode("utf-8"), spam='eggs')
        while not res.done():
            time.sleep(0.05)
        return res.done()
