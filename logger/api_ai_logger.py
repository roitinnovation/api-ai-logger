from logger.publisher import Publisher
from functools import wraps
from datetime import datetime
from flask import request
from threading import Thread
from settings import Settings
import traceback
import uuid

def get_logging(message):
    @wraps(message)
    def wrapper(*args, **kwrds):
        data = {}
        cfg = Settings()
        publisher = Publisher(cfg)
        payload_in = request.get_json(force=True)
        date_init = str(datetime.utcnow().timestamp())
        unknown_error = None
        try:
            payload_out, code = message(*args, **kwrds)
            error = True if payload_out['error'] is not None else False
        except Exception as e:
            payload_out = traceback.format_exc()
            error = True
            unknown_error = e
        date_finish = str(datetime.utcnow().timestamp())
        id = str(uuid.uuid4())
        print(f"LOG[INFO] ID from request: {id}")
        data['id'] = id
        data['payloadIn'] = payload_in
        data['payloadOut'] = payload_out
        data['dateInit'] = date_init
        data['dateFinish'] = date_finish
        data['version'] = cfg.version
        data['error'] = error
        data['environment'] = cfg.env
        data['applicationName'] = cfg.app_name
        data['uniqueIdentifier'] = payload_in.get('uniqueIdentifier')
        data['route'] = str(request.path).split('/')[-1]
        Thread(target=publisher.push, args=[data]).start()
        if unknown_error is not None:
            raise unknown_error
        return payload_out, code
    return wrapper

