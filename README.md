# Logger

### How to save roit AI API's loggings

First of all you need to have 2 file's, config/environment.json and settings.py

in this file's you will need at least those informations.

* config/environment.json
    
 ```

    {
        "prod": {
            "app_name": "api-canceled-nfse-classifier",
            "logging_topic": "projects/roit-ai-production/topics/api-ai-prod"
        },
    
        "hom": {
            "app_name": "api-canceled-nfse-classifier",
            "logging_topic": "projects/roit-ai-homologation/topics/api-ai-hom"
        },
    
    
        "dev": {
            "app_name": "api-canceled-nfse-classifier",
            "logging_topic": "projects/roit-ai-development/topics/api-ai-dev"
        }
     }

 ``` 

* settings.py

```
def set_env(self, env):
    with open("./config/environment.json") as sc:
        config = json.load(sc)
    
    self.env = env
    self.logging_topic =  config[env]['logging_topic']
    self.app_name = config[env]['app_name']
```

and set the version of API in settings.py file too.
```
def set_version(self, version):
    self.version = version
```

after to install api-ai-logger
```
pip install git+https://bitbucket.org/roitinnovation/api-ai-logger.git
```
before start your service you need to export credential, like

```
export GOOGLE_APPLICATION_CREDENTIALS="/home/user/Downloads/my-key.json"
```

and to use it, just import in your service file and use as a wrapper
```
from logger.api_ai_logger import get_logging

class Example(Resource):
    def __init__(self):
        pass

    def get(self):
        return {"message": "GET method not supported"}, 401

    @get_logging
    def post(self):

```

to see API's documentation look at our best practices repository